# -*- coding: utf-8 -*-
"""
connect resistor and led  to  boad pin 8 and run this script
whenever you say wow , the led shoulbe flash 
"""
import json
import os
import sounddevice as sd
import numpy as np
import scipy.signal
import timeit
import python_speech_features
import RPi.GPIO as GPIO
from subprocess import call
import shlex, subprocess
from tflite_runtime.interpreter import Interpreter
#from mechanize import Browser
from datetime import datetime
import requests

#from espeak import espeak

#import sys
#from time import sleep
#import pyttsx3 as pyttsx
#engine = pyttsx.init()
#voices = engine.getProperty('voices')
#print(voices)
#engine.setProperty('voice', voices[0].id)  # changes the voice


#engine.setProperty('voice', voices[14].id)  # changes the voice
#voiceNum=0
#Fr=[0 1 6 7 8 14] #6 = AC

now = datetime.now()

current_time = now.strftime("%H:%M:%S")
print("Current Time =", current_time)

# Configure the API key
wit_ai_api_key = os.getenv("WITAI_API_KEY")


# Param vicuna via python3 -m llama_cpp.server
url_vicuna = "http://192.168.1.95:8000/v1/chat/completions"
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}


#Parameters
debug_time=0
debug_acc=0
led_pin=8
word_threshold=0.1
rec_duration=0.5
windows_stride=0.5
sample_rate=48000
resample_rate=8000
num_channels=1
num_mfcc=16

#model_path='wake_word_loulou_model.tflite'
model_path='wake_word_lola_modelite.tflite'
my_exit=False

#Slide windows
windows = np.zeros(int(rec_duration * resample_rate) * 2)

#GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(8, GPIO.OUT, initial=GPIO.LOW)

#Load model interpreter
interpreter = Interpreter(model_path)
interpreter.allocate_tensors()
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
print(input_details)

def enreg_file(sentence):
	f = open("/home/pi/lola/Q_A.txt", "a")
	now = datetime.now()

	current_time = now.strftime("%Y/%m/%d %H:%M:%S")

	print("-> "+sentence)
	f.write(current_time  + "- >" + sentence+'-\n')
	f.close()

#decimal ' filter and downsample '
def decimate(signal, old_fs, new_fs):
	
	#Check to make sure we're downsampling
	if new_fs > old_fs:
		print("error: target sample rate higher than original ")
		return signal, old_fs
	
	#we can only donwsample by an integer factor
	dec_factor = old_fs / new_fs
	if not dec_factor.is_integer():
		print("error: can only decimal by integer factor")
		return signal, old_fs
	
	#Do decimation
	resample_signal = scipy.signal.decimate(signal, int(dec_factor))
	
	return resample_signal, new_fs


def say(something, voice='mb-fr4'):
    #print("espeak -v {} {}".format(voice,something))
    #espeak -a 200 -v mb-fr1 -s 150 "Les framboises sont perchées sur le tabouret de mon grand-père." --stdout | apl
    #somethingR = "Voici la réponse que propose CHATGT."+something
    somethingR = " "+something.replace("\u201C"," ").replace("\u201D"," ").replace("\u2019"," ").replace("\u2013"," ").replace("\u2014"," ").replace("\u0153"," ")
    somethingR = " "+something.replace("\u0153"," ")
    #somethingR = unicode(somethingR , "utf-8")
    somethingR = somethingR.encode('utf-8')
    subprocess.call(['/home/pi/lola/dit.sh',somethingR])    
    #subprocess.call(['espeak', '-s 125', '-v%s' % (voice), somethingR])
    #subprocess.call(['espeak', '-v%s' % (voice), somethingR])

	
def gest_intent(prompt):
	prompt = prompt.replace(",", " ")
	prompt = prompt.replace("\n"," ")
        # Generate some text
	#prompt = "quelle heure est il en france?"
	data = {"messages": [{"role": "system","content": "réponds en Français à la question"},{"role": "user","content": prompt }]}
	response = requests.post(url_vicuna, json=data, headers=headers)
	if response.status_code == 200:
		jsonResponse = response.json()
		myresponse = jsonResponse['choices'][0]['message']['content']
	else :
        	myresponse = response.status_code 
        # Get the first response
	message = myresponse
	#print(message)
	message = message.replace("\n", " ")
	enreg_file("A:"+message)
	say(message)
	
	
#This gets called every 0.5 seconds
def sd_callback(rec, frames, time, status):
	
	GPIO.output(led_pin, GPIO.LOW)
	
	#Start timing for testing
	start = timeit.default_timer()
	
	# Notify if error
	if status:
		print('error:', status)
	
	#REMOVE 2nd dimension from recording sample
	rec = np.squeeze(rec)
	
	rec, new_fs = decimate(rec, sample_rate, resample_rate)
	
	# resample
	windows[:len(windows)//2] = windows[len(windows)//2:]
	windows[len(windows)//2:] = rec

	#compute features 
	mfccs = python_speech_features.base.mfcc(windows,
						samplerate=new_fs,
						winlen=0.256,
						winstep=0.050,
						numcep=num_mfcc,
						nfilt=26,
						nfft=2048,
						preemph=0.0,
						ceplifter=0,
						appendEnergy=False,
						winfunc=np.hanning)

	mfccs = mfccs.transpose()
	
	#MAKE PREDICTION  FROM MODEL
	in_tensor = np.float32(mfccs.reshape(1, mfccs.shape[0], mfccs.shape[1], 1))
	interpreter.set_tensor(input_details[0]['index'], in_tensor)
	interpreter.invoke()
	output_data = interpreter.get_tensor(output_details[0]['index'])
	val = output_data[0][0]
	#data = input("saisie 'go' :\n")
    	#print(f'Processing Message from input() *****{data}*****')
	#if 'go' == data:	
	if val > word_threshold:
		print('lola')
		#GPIO.output(led_pin, GPIO.HIGH)
		#my_exit=True 
		print(val)
		call(["aplay", "/home/pi/lola/ready_ping.wav"])
		call(["arecord", "--format=S16_LE", "--duration=4", "--rate=16000", "--file-type=wav", "/home/pi/lola/myrec.wav"])
		os.system("curl -XPOST -H \"Authorization: Bearer "+wit_ai_api_key+"\" -H \"Content-Type: audio/wav\" \"https://api.wit.ai/speech?v=20220410\" --data-binary @myrec.wav > myret.json")
		call(["aplay", "/home/pi/lola/ready_ping.wav"])
		 
		with open("/home/pi/lola/myret.json") as f:
    		#with open("/bkp2/projects/tf/myret.json") as f:
			lines = f.readlines()
 		 

		for line in lines:
  			if line.find("text") > 0 :
  				line_ = line.replace("text", "").replace("\"", "").replace(":", "").replace(","," ").replace("\n"," ")
  				#print(line_)
		
		enreg_file("Q:"+line_)
		if line_ != "     " :
			gest_intent(line_)
    		
    		
	if debug_acc:
		print(val)
		#print(word_threshold)
	if debug_time:
		print(timeit.default_timer() - start)

#Start streaming from microphone
with sd.InputStream(channels=num_channels,
			samplerate=sample_rate,
			blocksize=int(sample_rate * rec_duration),
			callback=sd_callback):
	while (not my_exit) :
		pass





 


























































