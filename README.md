# Lola

# DIY a wake word box "Lola" with a Raspberry pi / wit.ai / chatgtp or Vicuna or ... / pico2wave or espeak

All the source code "ready to run" is here. 
in audio : usb dongle microphone 
out audio : speaker

## Installation

find out the tokens proposed via wit.ai and openai.

and .bashrc it like :
```bash
export OPENAI_API_KEY=xx-ddfsDFGDFGDFGTSDFGEJFGGQDFGdsfghsfghsfghdfghsfhf
export WITAI_API_KEY=XXGQSDFKJGKQJGSD35435434DSFLKHS                     
```
my .bash_history concerning pip install for requirements.txt: 
```bash
pip install https://github.com/bitsy-ai/tensorflow-arm-bin/releases/download/v2.4.0-rc2/tensorflow-2.4.0rc2-cp37-none-linux_armv7l.whl
pip install https://storage.googleapis.com/tensorflow/raspberrypi/tensorflow-2.3.0-cp35-none-linux_armv7l.whl
pip install tensorflow
pip install sounddevice
pip install scipy
pip install numpy
pip install python_speech_features
pip install tflite-runtime
pip install playsound
pip install pygobject
pip install gst
pip install pygame
pip install libSDL2_mixer
pip install sounddevice
pip install mechanize
pip install openai
pip install python-espeak
pip install pyttsx3
```

```bash
cd /home/pi 
pip install -r requirements.txt
git clone https://gitlab.com/lolo2mars/lola.git
wget http://ftp.us.debian.org/debian/pool/non-free/s/svox/libttspico0_1.0+git20130326-9_armhf.deb 
wget http://ftp.us.debian.org/debian/pool/non-free/s/svox/libttspico-utils_1.0+git20130326-9_armhf.deb 
sudo apt-get install -f ./libttspico0_1.0+git20130326-9_armhf.deb ./libttspico-utils_1.0+git20130326-9_armhf.deb 
```

## Usage

with OPENAI:

python3 client_gpt3.py

"Lola"->Ding->N sec for your question->Ding->chatgpt->synth voice->Q_A.txt log yymmdd hh:mm:ss Q/A
 

without OPENAI:

python3 client_vicuna.py

"Lola"->Ding->N sec for your question->Ding->FastApi server Vicuna or others->synth voice->Q_A.txt log yymmdd hh:mm:ss Q/A

edit the script to update FastApi server ip

FastApi server from https://github.com/abetlen/llama-cpp-python


## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

All code in this repository is for demonstration purposes and licensed under Beerware 
( https://en.wikipedia.org/wiki/Beerware ). so https://ko-fi.com/lola83   
Distributed as-is; no warranty is given. 

the Mentors are :

https://www.youtube.com/watch?v=0fn7pj7Dutc
 
https://www.youtube.com/watch?v=re-dSV_a0tM

https://www.framboise314.fr/donnez-la-parole-a-votre-raspberry-pi/

https://community.jeedom.com/t/e-le-paquet-libttspico-utils-na-pas-de-version-susceptible-detre-installee/79901/3  


Friend project : 

https://github.com/MysticXtheUnknown/voicechatai-rpi-x64-russ?fbclid=IwAR0Pq0g-czw-QnEjs6lCBFJMVgbPbFVp8Bbb0Y-oBmgG4ld9MYkAaQoZrYM


